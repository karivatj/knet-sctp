/* Copyright 2010 Jukka Jyl�nki, Ville Saarinen, Kari Vatjus-Anttila

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

/** @file SCTPMessageConnection.cpp
	@brief */

#include <sstream>
#include <netinet/in.h>
#include <netinet/sctp.h>

#ifdef KNET_USE_BOOST
#include <boost/thread/thread.hpp>
#endif
#include "kNet/Allocator.h"

#include "kNet/DebugMemoryLeakCheck.h"
#include "kNet/SCTPMessageConnection.h"
#include "kNet/NetworkLogging.h"
#include "kNet/DataSerializer.h"
#include "kNet/DataDeserializer.h"
#include "kNet/VLEPacker.h"
#include "kNet/NetException.h"
#include "kNet/Network.h"

namespace kNet
{

/// The maximum size for a SCTP message we will allow to be received. If we receive a message larger than this, we consider
/// it as a protocol violation and kill the connection.
static const u32 cMaxReceivableSCTPMessageSize = 1024 * 1024;

SCTPMessageConnection::SCTPMessageConnection(Network *owner, NetworkServer *ownerServer, Socket *socket, ConnectionState startingState)
:MessageConnection(owner, ownerServer, socket, startingState), sctpInboundSocketData(64 * 1024)
{
    struct sockaddr *addrs;
    seconds = time(NULL);
    prev_addr.addr_count = sctp_getladdrs(socket->GetSocketHandle(), 0, &addrs);
    memcpy(prev_addr.addrs, addrs, prev_addr.addr_count * sizeof(struct sockaddr));
    sctp_freeladdrs(addrs);
}

SCTPMessageConnection::~SCTPMessageConnection()
{
	if (owner) owner->CloseConnection(this);
}

MessageConnection::SocketReadResult SCTPMessageConnection::ReadSocket(size_t &totalBytesRead)
{
	AssertInWorkerThreadContext();

	totalBytesRead = 0;

	if (!socket || !socket->IsReadOpen())
		return SocketReadError;

	using namespace std;

	// This is a limit on how many messages we keep in the inbound application buffer at maximum.
	// If we receive data from the SCTP socket faster than this limit, we stop reading until
	// the application handles the previous messages first.
	const int arbitraryInboundMessageCapacityLimit = 2048;

	if (inboundMessageQueue.CapacityLeft() < arbitraryInboundMessageCapacityLimit) 
	{
		LOG(LogVerbose, "SCTPMessageConnection::ReadSocket: Read throttled! Application cannot consume data fast enough.");
		return SocketReadThrottled; // Can't read in new data, since the client app can't process it so fast.
	}

	// This is an arbitrary throttle limit on how much data we read in this function at once. Without this limit, 
	// a slow computer with a fast network connection and a fast sender at the other end could flood this end 
	// with so many messages that we wouldn't ever return from the loop below until the sender stops. This would
	// starve the processing of all other connections this worker thread has to manage.
	const size_t maxBytesToRead = 1024 * 1024;

	// Pump the socket's receiving end until it's empty or can't process any more for now.
	while(totalBytesRead < maxBytesToRead)
	{
		assert(socket);

		// If we don't have enough free space in the ring buffer (even after compacting), throttle the reading of data.
		if (sctpInboundSocketData.ContiguousFreeBytesLeft() < 16384 && sctpInboundSocketData.Capacity() > 1048576)
		{
			sctpInboundSocketData.Compact();
			if (sctpInboundSocketData.ContiguousFreeBytesLeft() < 16384)
				return SocketReadThrottled;
		}

		OverlappedTransferBuffer *buffer = socket->BeginReceive();
		if (!buffer)
			break; // Nothing to receive.

		// If we can't fit the data we got, compact the ring buffer.
		if (buffer->bytesContains > sctpInboundSocketData.ContiguousFreeBytesLeft())
		{
			sctpInboundSocketData.Compact();
			if (buffer->bytesContains > sctpInboundSocketData.ContiguousFreeBytesLeft())
			{
				// Even compacting didn't get enough space to fit the message, so resize the ring buffer to be able to contain the message.
				// At least always double the capacity of the buffer, so that we don't waste effort incrementing the capacity by too small amounts at a time.
				sctpInboundSocketData.Resize(max(sctpInboundSocketData.Capacity()*2, sctpInboundSocketData.Capacity() + buffer->bytesContains - sctpInboundSocketData.ContiguousFreeBytesLeft()));
				LOG(LogWaits, "SCTPMessageConnection::ReadSocket: Performance warning! Resized the capacity of the receive ring buffer to %d bytes to accommodate a message of size %d (now have %d bytes of free space)",
					sctpInboundSocketData.Capacity(), buffer->bytesContains, sctpInboundSocketData.ContiguousFreeBytesLeft());
			}
		}

			//LOG(LogInfo, "SCTPMessageConnection::ReadSocket: Received %d bytes from the network from peer %s.",
			//buffer->bytesContains, socket->ToString().c_str());

		assert((size_t)buffer->bytesContains <= (size_t)sctpInboundSocketData.ContiguousFreeBytesLeft());
		///\todo For performance, this memcpy can be optimized away. We can parse the message directly
		/// from this buffer without copying it to a temporary working buffer. Detect if message straddles
		/// two OverlappedTransferBuffers and only in that case memcpy that message to form a
		/// single contiguous memory area.
		memcpy(sctpInboundSocketData.End(), buffer->buffer.buf, buffer->bytesContains);
		sctpInboundSocketData.Inserted(buffer->bytesContains); // Mark the memory area in the ring buffer as used.

		totalBytesRead += buffer->bytesContains;
		socket->EndReceive(buffer);
	}

	// Update statistics about the connection.
	if (totalBytesRead > 0)
	{
		lastHeardTime = Clock::Tick();
		ADDEVENT("sctpDataIn", (float)totalBytesRead, "bytes");
		AddInboundStats(totalBytesRead, 1, 0);
	}

	// Finally, try to parse any bytes we received to complete messages. Any bytes consisting a partial
	// message will be left into the sctpInboundSocketData partial buffer to wait for more bytes to be received later.
	ExtractMessages();

	if (totalBytesRead >= maxBytesToRead)
		return SocketReadThrottled;
	else
		return SocketReadOK;
}

/// Checks that the specified conditions for the container apply.
/// Warning: This is a non-threadsafe check for the container, only to be used for debugging.
/// Warning #2: This function is very slow, as it performs a N^2 search through the container.
template<typename T>
bool ContainerUniqueAndNoNullElements(const std::vector<T> &cont)
{
	for(size_t i = 0; i < cont.size(); ++i)
		for(size_t j = i+1; j < cont.size(); ++j)
			if (cont[i] == cont[j] || cont[i] == 0)
				return false;
	return true;
}

/// Packs several messages from the outbound priority queue into a single packet and sends it out the wire.
/// @return False if the send was a failure and sending should not be tried again at this time, true otherwise.
MessageConnection::PacketSendResult SCTPMessageConnection::SendOutPacket()
{
	AssertInWorkerThreadContext();

    if (bOutboundSendsPaused || outboundQueue.Size() == 0)
        return PacketSendNoMessages;

    if (!socket || !socket->IsWriteOpen())
    {
        LOG(LogVerbose, "SCTPMessageConnection::SendOutPacket: Socket is not write open %p!", socket);
        if (connectionState == ConnectionOK) ///\todo This is slightly annoying to manually update the state here,
            connectionState = ConnectionPeerClosed; /// reorganize to be able to have this automatically apply.
        if (connectionState == ConnectionDisconnecting)
            connectionState = ConnectionClosed;
        return PacketSendSocketClosed;
    }

    // 'serializedMessages' is a temporary data structure used only by this member function.
    // It caches a list of all the messages we are pushing out during this call.
    serializedMessages.clear();

    // In the following, we start coalescing multiple messages into a single socket send() calls.
    // Get the maximum number of bytes we can coalesce for the send() call. This is only a soft limit
    // in the sense that if we encounter a single message that is larger than this limit, then we try
    // to send that through in one send() call.
//	const size_t maxSendSize = socket->MaxSendSize();

    // Push out all the pending data to the socket.
    OverlappedTransferBuffer *overlappedTransfer = 0;

    int numMessagesPacked = 0;
    DataSerializer writer;
//	assert(ContainerUniqueAndNoNullElements(outboundQueue)); // This precondition should always hold (but very heavy to test, uncomment to debug)
    while(outboundQueue.Size() > 0)
    {
#ifdef KNET_NO_MAXHEAP
        NetworkMessage *msg = *outboundQueue.Front();
#else
        NetworkMessage *msg = outboundQueue.Front();
#endif

        if (msg->obsolete)
        {
            ClearOutboundMessageWithContentID(msg);
            FreeMessage(msg);
            outboundQueue.PopFront();
            continue;
        }

        const int encodedMsgIdLength = VLE8_16_32::GetEncodedBitLength(msg->id) / 8;
        const size_t messageContentSize = msg->dataSize + encodedMsgIdLength; // 1 byte: Message ID. X bytes: Content.
        const int encodedMsgSizeLength = VLE8_16_32::GetEncodedBitLength(messageContentSize) / 8;
        const size_t totalMessageSize = messageContentSize + encodedMsgSizeLength; // 2 bytes: Content length. X bytes: Content.

        if (!overlappedTransfer)
        {
            overlappedTransfer = socket->BeginSend(std::max<size_t>(socket->MaxSendSize(), totalMessageSize));
            if (!overlappedTransfer)
            {
                LOG(LogError, "SCTPMessageConnection::SendOutPacket: Starting an overlapped send failed!");
                assert(serializedMessages.size() == 0);
                return PacketSendSocketClosed;
            }
            writer = DataSerializer(overlappedTransfer->buffer.buf, overlappedTransfer->buffer.len);
        }

        // If this message won't fit into the buffer, send out all previously gathered messages.
        if (writer.BytesLeft() < totalMessageSize)
            break;

        writer.AddVLE<VLE8_16_32>(messageContentSize);
        writer.AddVLE<VLE8_16_32>(msg->id);
        if (msg->dataSize > 0)
            writer.AddAlignedByteArray(msg->data, msg->dataSize);
        ++numMessagesPacked;

        serializedMessages.push_back(msg);
#ifdef KNET_NO_MAXHEAP
        assert(*outboundQueue.Front() == msg);
#else
        assert(outboundQueue.Front() == msg);
#endif
        outboundQueue.PopFront();
    }
//	assert(ContainerUniqueAndNoNullElements(serializedMessages)); // This precondition should always hold (but very heavy to test, uncomment to debug)

    if (writer.BytesFilled() == 0 && outboundQueue.Size() > 0)
        LOG(LogError, "Failed to send any messages to socket %s! (Probably next message was too big to fit in the buffer).", socket->ToString().c_str());

    overlappedTransfer->bytesContains = writer.BytesFilled();
    bool success = socket->EndSend(overlappedTransfer);

    if (!success) // If we failed to send, put all the messages back into the outbound queue to wait for the next send round.
    {
        for(size_t i = 0; i < serializedMessages.size(); ++i)
#ifdef KNET_NO_MAXHEAP
            outboundQueue.InsertWithResize(serializedMessages[i]);
#else
            outboundQueue.Insert(serializedMessages[i]);
#endif
//		assert(ContainerUniqueAndNoNullElements(outboundQueue));

        LOG(LogError, "SCTPMessageConnection::SendOutPacket() failed: Could not initiate overlapped transfer!");

        return PacketSendSocketFull;
    }

    LOG(LogData, "SCTPMessageConnection::SendOutPacket: Sent %d bytes (%d messages) to peer %s.", (int)writer.BytesFilled(), (int)serializedMessages.size(), socket->ToString().c_str());
    AddOutboundStats(writer.BytesFilled(), 1, numMessagesPacked);
    ADDEVENT("sctpDataOut", (float)writer.BytesFilled(), "bytes");

    // The messages in serializedMessages array are now in the SCTP driver to handle. It will guarantee
    // delivery if possible, so we can free the messages already.
    for(size_t i = 0; i < serializedMessages.size(); ++i)
    {
#ifdef KNET_NETWORK_PROFILING
        std::stringstream ss;
        if (!serializedMessages[i]->profilerName.empty())
            ss << "messageOut." << serializedMessages[i]->profilerName;
        else
            ss << "messageOut." << serializedMessages[i]->id;
        ADDEVENT(ss.str().c_str(), (float)serializedMessages[i]->Size(), "bytes");
#endif
        ClearOutboundMessageWithContentID(serializedMessages[i]);
        FreeMessage(serializedMessages[i]);
    }

    return PacketSendOK;
}

void SCTPMessageConnection::DoUpdateConnection() // [worker thread]
{
        if(socket->Type() == ClientSocket)
        {
            time_t temp;

            if((temp = time(NULL) ) > seconds + 2)  // we don't want to do this every time the worker thread gets here
            {
                seconds = temp;
                UpdateAddresses();
            }
        }

	ExtractMessages();
}

/// Updates the addresses of the client or the server and adds them to the SCTP association. Note that addresses are currently fetched
/// manually since lksctp implementation doesn't seem to support automatically dynamic address reconfiguration.
void SCTPMessageConnection::UpdateAddresses()
{
        int i, j;
        bool add_this = false;

        current_addr.addr_count = SCTPMessageConnection::GetIPAddresses(socket->GetSocketHandle(), current_addr);

        for(i = 0; i < current_addr.addr_count; i++)
        {
                add_this = true;

                for(j = 0; j < prev_addr.addr_count; j++)
                {
                    if(memcmp(&current_addr.addrs[i].sin_addr, &prev_addr.addrs[j].sin_addr, sizeof(struct in_addr)) == 0)// compare new addresses to previous addresses
                    {
                            add_this = false;                                                                                  // if address is one of the previous ones, address is already bound
                            break;
                    }
                }

                if(add_this)
                {
                    if(sctp_bindx(socket->GetSocketHandle(),(struct sockaddr*)(current_addr.addrs + i), 1, SCTP_BINDX_ADD_ADDR) < 0)    // bind new address
                            LOG(LogError, "Can't bind new address for SCTP association");
                }
        }

        memset(&prev_addr, 0, sizeof(prev_addr));               // save the addresses for the next time
        memcpy(&prev_addr, &current_addr, sizeof(addresses));
}

void SCTPMessageConnection::SendOutPackets()
{
	AssertInWorkerThreadContext();

	if (!socket || !socket->IsWriteOpen() || !socket->IsOverlappedSendReady())
		return;

	PacketSendResult result = PacketSendOK;
	int maxSends = 500; // Place an arbitrary limit to how many packets we will send at a time.
	while(result == PacketSendOK && maxSends-- > 0)
		result = SendOutPacket();

	// Thread-safely clear the eventMsgsOutAvailable event if we don't have any messages to process.
	if (NumOutboundMessagesPending() == 0)
		eventMsgsOutAvailable.Reset();
	if (NumOutboundMessagesPending() > 0)
		eventMsgsOutAvailable.Set();
}

void SCTPMessageConnection::ExtractMessages()
{
	AssertInWorkerThreadContext();

	try
	{
		size_t numMessagesReceived = 0;
		for(;;)
		{
			if (sctpInboundSocketData.Size() == 0) // No new packets in yet.
				break;

			if (inboundMessageQueue.CapacityLeft() == 0) // If the application can't take in any new messages, abort.
				break;

			DataDeserializer reader(sctpInboundSocketData.Begin(), sctpInboundSocketData.Size());
			u32 messageSize = reader.ReadVLE<VLE8_16_32>();
			if (messageSize == DataDeserializer::VLEReadError)
				break; // The packet hasn't yet been streamed in.

			if (messageSize == 0 || messageSize > cMaxReceivableSCTPMessageSize)
			{
				LOG(LogError, "Received an invalid message size %d!", (int)messageSize);
				throw NetException("Malformed SCTP data! Received an invalid message size!");
			}

			if (reader.BytesLeft() < messageSize)
				break; // We haven't yet received the whole message, have to abort parsing for now and wait for the whole message.

			HandleInboundMessage(0, reader.CurrentData(), messageSize);
			reader.SkipBytes(messageSize);

			assert(reader.BitPos() == 0);
			u32 bytesConsumed = reader.BytePos();

			// Erase the bytes we just processed from the ring buffer.
			sctpInboundSocketData.Consumed(bytesConsumed);

			++numMessagesReceived;
		}
		AddInboundStats(0, 0, numMessagesReceived);
	} catch(const NetException &e) {
		LOG(LogError, "SCTPMessageConnection::ExtractMessages() caught a network exception: \"%s\"!", e.what());
		if (socket)
			socket->Close();
		connectionState = ConnectionClosed;
	}
}

void SCTPMessageConnection::PerformDisconnection()
{
	AssertInMainThreadContext();

        if(socket)
            socket->Disconnect();
}

void SCTPMessageConnection::DumpConnectionStatus() const
{
	AssertInMainThreadContext();

	char str[2048];
	sprintf(str,
		"\tsctpInboundSocketData.Capacity(): %d\n"
		"\tsctpInboundSocketData.Size(): %d\n"
		"\tsctpInboundSocketData.ContiguousFreeBytesLeft(): %d\n",
		sctpInboundSocketData.Capacity(), // Note: This accesses a shared variable from the worker thread in a thread-unsafe way, and can crash. Therefore only use this function for debugging.
		sctpInboundSocketData.Size(),
		sctpInboundSocketData.ContiguousFreeBytesLeft());

	LOGUSER(str);
}

unsigned long SCTPMessageConnection::TimeUntilCanSendPacket() const
{
	return 0; //Not used
}

int SCTPMessageConnection::GetIPAddresses(SOCKET socket, addresses &addr)
{
    struct ifreq ifr[MAX_ADDRESSES];
    struct ifconf ifconf;
    int i, j;

    memset(&ifconf, 0, sizeof(struct ifconf));
    ifconf.ifc_req = ifr;
    ifconf.ifc_len = sizeof(ifr);

    if(ioctl(socket, SIOCGIFCONF, &ifconf) < 0)              //Get all IP-Addresses
        LOG(LogError, "Error while retrieving addresses in SCTP association.");

    for(i = 0, j = 0; i < ifconf.ifc_len/sizeof(struct ifreq); i++, j++)
    {
        if(strcmp(inet_ntoa(((struct sockaddr_in*)&ifconf.ifc_req[i].ifr_addr)->sin_addr), "127.0.0.1") != 0)//We don't need localhost address
        {
             addr.addrs[j] = *(struct sockaddr_in*)&ifconf.ifc_req[i].ifr_addr;
        }

        else j--;
    }

    return j;   // this holds the number of retrieved addresses
}

} // Namespace kNet
