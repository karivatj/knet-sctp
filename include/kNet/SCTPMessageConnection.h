/* Copyright 2011 Jukka Jyl�nki

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */
#ifndef SCTPMESSAGECONNECTION_H
#define SCTPMESSAGECONNECTION_H

/** @file SCTPMessageConnection.h
	@brief The SCTPMessageConnection class.*/

#include "MessageConnection.h"
#include "RingBuffer.h"
#ifdef UNIX
#include <netdb.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#endif
/*
TODO: SCTP stream format:
*/

#define MAX_ADDRESSES 10

namespace kNet
{

typedef struct {

    int addr_count;
    sockaddr_in addrs[MAX_ADDRESSES];

}addresses;


class SCTPMessageConnection : public MessageConnection
{
public:
	SCTPMessageConnection(Network *owner, NetworkServer *ownerServer, Socket *socket, ConnectionState startingState);
	~SCTPMessageConnection();

    static int GetIPAddresses(SOCKET socket, addresses &addr);

private:
	/// Maintains a byte buffer that contains partial messages. [worker thread]
	RingBuffer sctpInboundSocketData;

	/// Reads all available bytes from a stream socket.
	SocketReadResult ReadSocket(size_t &bytesRead); // [worker thread]

	PacketSendResult SendOutPacket(); // [worker thread]
	void SendOutPackets(); // [worker thread]

	void DoUpdateConnection(); // [worker thread]

        /// Updates the addresses of the client or the server.
        void UpdateAddresses();

	unsigned long TimeUntilCanSendPacket() const;

	/// Parses the raw inbound byte stream into messages. [used internally by worker thread]
	void ExtractMessages();

	// The following are temporary data structures used by various internal routines for processing.
	std::vector<NetworkMessage*> serializedMessages; // MessageConnection::SCTPSendOutPacket()

	void PerformDisconnection();

	void DumpConnectionStatus() const; // [main thread]

        addresses current_addr;     // this holds the addresses we currently should have bound in our association
        addresses prev_addr;        // these are the addresses we had when previously checked

        time_t seconds;
};

} // ~kNet
#endif /* SCTPMESSAGECONNECTION_H */
